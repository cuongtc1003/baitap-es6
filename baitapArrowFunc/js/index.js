// START BAI 1
const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];
var stock=colorList[0];
// TAO HTML CAC BUTTON
let createColorButton = () => {
    let buttonTag0=`<button id="${colorList[0]}" class="color-button ${colorList[0]} active" onclick="changeColor('${colorList[0]}')" ></button>`;
    let buttonList=buttonTag0;
    for (let i=1; i<colorList.length; i++){
        let buttonTag=`<button id="${colorList[i]}" class="color-button ${colorList[i]}" onclick="changeColor('${colorList[i]}')" ></button>`;
        buttonList=buttonList+buttonTag;
    };
    document.getElementById('colorContainer').innerHTML=buttonList;
};
// CHAY LAN DAU KHI LOAD TRANG
createColorButton();
// DOI MAU + THEM CLASS ACTIVE CHO TAG
let changeColor = (color) =>{
    deleteOldActive();
    stock=color;
    document.getElementById('house').classList.add(color);
    let element=document.getElementById(color);
    element.classList.add("active");    
};
// XOA TAG CO CLASS ACTIVE CU~
let deleteOldActive=()=>{
    let element=document.getElementById(stock);
    element.classList.remove("active");
};

